# Ciebit Organizador

Este pacote visa separar uma lista de arquivos em uma nova pasta pegando os arquivos ímpares de uma contagem realizada em sequência.

## Instalação

```
#!bash
npm install -g ciebit-organizador
```

## Utilização

```
#!bash
cborganizar nome-da-pasta
```

Ao rodar o comando acima uma pasta será criada e os arquivos ímpares serão movidos para ela.
