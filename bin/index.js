#! /usr/bin/env node

var FileSistem = require('fs');
var mkdirp = require('mkdirp');

var pastaProcessosNome = 'ciebit-organizador-movidos';
var pastaProcessosCaminho = process.argv[2] + '/' + pastaProcessosNome;

// Criando pasta
mkdirp(pastaProcessosCaminho, function(erro) {
    if (erro) {
        console.log(erro);
    }
});

// Lendo arquivos
FileSistem.readdir(process.argv[2], organizar);

function organizar(erros, arquivos){
    var contagem = 1;

    arquivos.forEach(function(arquivo){
        if (arquivo == pastaProcessosNome) {
            return;
        }

        if (contagem % 2) {
            FileSistem.rename(process.argv[2] +'/'+ arquivo, pastaProcessosCaminho +'/'+ arquivo);
            console.log(arquivo);
        }

        contagem++;
    })
}
